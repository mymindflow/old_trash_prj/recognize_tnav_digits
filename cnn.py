import cv2
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D, BatchNormalization
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ReduceLROnPlateau
from sklearn.model_selection import train_test_split


class MyExcept(Exception):
	pass

class NN:
	def __init__(self, batch_size = 64, num_classes = 10, epochs = 20, input_shape = (36, 300, 1)):
		self.batch_size = batch_size
		self.num_classes = num_classes
		self.epochs = epochs
		self.input_shape = input_shape
	
	@staticmethod	
	def preprocessing(image):
		image = image.astype('float32')  
		image /= 255
		if len(image.shape) == 3:
			image = cv2.resize(image, (36, 300))	
			image = image.reshape(1, 36, 300,1)
		elif len(image.shape) == 4:
			image = np.array(list(map(lambda x: cv2.resize(x, (36, 300)), image)))
			image = image.reshape(image.shape[0], 36, 300,1)
		else:
			raise MyExcept('не правильный размер')
		return image
	
	def build_model(self):	
		model = Sequential()
		model.add(Conv2D(32, kernel_size = (3, 12), activation = 'relu',kernel_initializer = 'he_normal', input_shape = self.input_shape))
		model.add(Conv2D(32, kernel_size = (3, 12), activation = 'relu',kernel_initializer = 'he_normal'))
		model.add(MaxPool2D((2, 8)))
		model.add(Dropout(0.2))
		model.add(Conv2D(64, (3, 12), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal'))
		model.add(Conv2D(64, (3, 12), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal'))
		model.add(MaxPool2D(pool_size = (2, 8)))
		model.add(Dropout(0.2))
		model.add(Conv2D(128, (3, 12), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal'))
		model.add(Dropout(0.2))
		model.add(Flatten())
		model.add(Dense(128, activation = 'relu'))
		model.add(BatchNormalization())
		model.add(Dropout(0.2))
		model.add(Dense(self.num_classes, activation = 'softmax'))
		
		return model

	def callback_lr():
		return ReduceLROnPlateau(monitor = 'val_acc', 
                                            patience = 3, 
                                            verbose = 1, 
                                            factor = 0.5, 
                                            min_lr = 0.0001)
                                                                                
	def data_gen(X_train):
		self.datagen = ImageDataGenerator(
			featurewise_center = False,  
			samplewise_center = False, 
			featurewise_std_normalization = False,  
			samplewise_std_normalization = False, 
			zca_whitening = False, 
			rotation_range = 15, 
			zoom_range = 0.1, 
			width_shift_range = 0.1,  
			height_shift_range = 0.1,  
			horizontal_flip = False,  
			vertical_flip = False)  
			
		self.datagen.fit(X_train)
		
		 
              
	def fit(X_train, y_train, test_size = 0.1, random_state = 42): 
		X_train = NN.preprocessing(X_train)
		y_train = keras.utils.to_categorical(y_train, self.num_classes)
		X_train, X_val, Y_train, Y_val = train_test_split(X_train, y_train, test_size = test_size, random_state = random_state)
		

		self.model = self.build_model()
		
		self.model.compile(loss = keras.losses.categorical_crossentropy,
              optimizer = keras.optimizers.RMSprop(),
              metrics = ['accuracy'])
		
		self.model.fit_generator(self.datagen.flow(X_train,Y_train, batch_size = self.batch_size),
                              epochs = self.epochs, validation_data = (X_val, Y_val),
                              verbose = 1, steps_per_epoch = X_train.shape[0] // self.batch_size, callbacks=[self.callback_lr()])
                              
	def save(self, name):
		assert isinstance(name, str), 'name must be str'
		try:
			model.save_weights(text + '.h5') 
		except MyException as exc:
			self.model.fit()
			model.save_weights(text + '.h5') 
                              
	def load(self, name):
		assert isinstance(name, str), 'name must be str'
		self.model = self.build_model()
		self.model.load_weights(name + '.h5')
	
	def predict(self, image):
		image = NN.preprocessing(image)
		return self.model.predict(image)
		
	def __repr__(self):
		return self.model.summary()

