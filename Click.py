# -*- coding: utf-8 -*-
from pynput import mouse, keyboard 
import numpy as np
from PIL import ImageGrab
from cv import FINDCONTOUR



class A:
    def __init__(self, prefix:str, i:int = 0):
        assert isinstance(prefix, str), 'value of prefix must be str'
        self.prefix = prefix
        self.i = i
        self.flag = False
        self.last = (None, None)
    def calculate_box(self, x, y):
        return (x, y, x + 55, y + 12)
    def __on_press(self, key):
        if key.ctrl_l: self.flag = True
            
    def __on_release(self, key): self.flag = False
    def show(self, img):
        ob = FINDCONTOUR(img)
        s = ob.draw_contour()
        return s
    def __on_click(self, x, y, button, pressed):
        if self.flag and pressed:
            if self.last != (x, y):
                image = np.array(ImageGrab.grab(bbox = self.calculate_box(72, 1024)))
                print(x, y)
                res = self.show(image)
                file = open('recognize.txt', 'wa')
                file.write('PERMX {} {} 1 16 100 100 100 MERGE \n'.format(*res.split('_')[:-1]))
                

                self.i += 1
            self.last = (x, y)
    def __enter__(self):
        with mouse.Listener(on_click = self.__on_click) as listener:
            with keyboard.Listener(on_press = self.__on_press, on_release = self.__on_release) as self.listener:
                listener.join()
                self.show()
    def __exit__(self, **args):
        pass

        
        
		
with A('s') as f:
	f
		
		
