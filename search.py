# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 11:39:03 2019

@author: BadmaevOO
"""
import cv2
from PIL import ImageGrab

def calculate_box(x, y):
        return (x, y, x + 1000, y + 12)

img = np.array(ImageGrab.grab(bbox = calculate_box(0, 1024)))
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img = cv2.resize(img, (3000, 300)) 
img = cv2.GaussianBlur(img, (15, 15), 0)
img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
get_box = lambda x: (x, 110, x + 17, 285)
x = 0
eps = 1
while x <= img.shape[0]:
    box = get_box(x)
    cv2.rectangle(img, box[:2], box[2:], (0, 255, 0), 2)
    x += eps
    cv2.imshow('draw contours: {}'.format(img), img)
    cv2.waitKey(0)