# -*- coding: utf-8 -*-
import numpy as np
import cv2
from collections import Counter
from cosin import DIGITS

class FINDCONTOUR:
    def __init__(self, img):
        self.img = img
    def preprocessing(self, img):
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.resize(img, (300, 300)) 
        img = cv2.GaussianBlur(img, (15, 15), 0)
        img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
        _, thresh = cv2.threshold(img, 0, 255,  cv2.THRESH_BINARY, cv2.THRESH_OTSU)
        contours, _ =  cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        bound = list(map(lambda x: (x[0], x[1], x[0] + x[2], x[1] + x[3]), map(cv2.boundingRect, contours))) #x, y, x + w, y + h 
        bound.sort(key = lambda x: x[0]) # сортировка, чтобы контура шли  слево на право 
        bound = list(filter(lambda contour: (contour[2] - contour[0]) > 10, bound))
        f = lambda bound, ind: sorted(tuple(Counter([y[ind] for y in bound]).items()), key = lambda x: x[1], reverse = True)[0][0]
        bottom = f(bound, 3)
        top = f(bound, 1)
        check_iter = lambda it, val: any(elem == val for elem in it)
        #выбираем нужные контуры
        find_number = []
        for contour in bound:
            #условия на границу изображения
            if check_iter(contour[slice(0, 3, 2)], img.shape[1]) or check_iter(contour[slice(1, 4, 2)], img.shape[0]): continue
            elif (contour[2] - contour[0]) > 50:
                left = contour[0]
                rigth = contour[2]
                while abs(rigth - left) > 33:
                    find_number.append((left, contour[1], left + 33, contour[3]))
                    left = contour[0] + 33  
                find_number.append((left, contour[1], left + 33, contour[3]))
        #ограничение верхней линией или нахождение кругов внутри
            elif (bottom - top) / (contour[3]- contour[1]) > 1.1:
                if abs(contour[1] - top) < 3 and 28 < contour[2] - contour[0] < 33: 
                    find_number.append((contour[0], contour[1], contour[2], contour[1] + 240))
                else: 
                    continue
            else: 
                find_number.append(contour)
        return thresh, find_number, img
    
    def draw_contour(self):
        #img = cv2.imread('image\{}'.format(path_img))
        img = self.img.copy()
        thresh, find_number, img = self.preprocessing(img)
        first = None
        second = None
        summa = 0
        s = ''
        for i, contour in enumerate(find_number):  
            number_image = thresh[contour[1]: contour[3], contour[0]: contour[2]] #[y:y + h, x: x + w]
            if len(np.where(number_image == 255)[0]) / len(np.where(number_image == 0)[0]) > 3: continue 
            #plt.imsave('digits\{}_{}.jpg'.format(path_img[:-4], i),cv2.resize(number_image, (28, 28)))    
            label = DIGITS().predict(cv2.resize(number_image, (28, 28)))
            cv2.rectangle(img, contour[:2], contour[2:], (0, 255, 0), 2)
            cv2.putText(img, str(label), contour[:2], cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2) 
            if 1 <= i < (len(find_number)):
                first = find_number[i - 1][2]
                second = find_number[i][0]
                if abs(second - first) >= 15:
                    s += '_'
                    summa += 1
                    if summa == 2: break
            s += '{}'.format(label) 

        print(s)
        cv2.imshow('draw contours: {}'.format(self.img), img)
        cv2.waitKey(0)
        return s
        


