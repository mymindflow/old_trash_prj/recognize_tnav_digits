from collections import namedtuple
import cv2
import numpy as np
from scipy.spatial.distance import cosine

class MyExcept(Exception):
    pass


class DIGITS:
    def __init__(self):
        self.digits = np.array([np.array(cv2.cvtColor(cv2.imread('digits\s_{}.jpg'.format(i)), cv2.COLOR_BGR2GRAY)) for i in range(10)])
    def predict(self, img):
        pred = []
        for digit in self.digits:
            pred.append(cosine(digit.flatten(), np.array(img).flatten()))
        return np.argmax(pred)
    def draw(self):
        for i in range(len(self.digits)): cv2.imshow('draw contours: {}'.format(i), self.digits[i])
        cv2.waitKey(0)

        




